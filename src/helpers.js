var PIECES = {
    x: 'X',
    o: 'O',
    empty: '-'
};

module.exports.PIECES = PIECES;
