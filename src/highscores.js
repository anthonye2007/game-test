var jsonfile = require('jsonfile');

function HighScores(filePath) {
    filePath = filePath || './highScores.json';

    this.highScores = [{
        name: "Player One",
        score: 0
    }];

    var getHighScores = function(callback) {
        return new Promise((resolve, reject) => {
            jsonfile.readFile(filePath, function(err, scoresFromFile) {
                if (err) {
                    console.error("Could not read high scores file " + err);
                    reject(err);
                }
                else {
                    resolve(scoresFromFile);
                }
            });
        });
    };

    var prettyPrint = function(scoresArray) {
        scoresArray = scoresArray || this.highScores;
        scoresArray.sort((a, b) => {
            return a.score - b.score;
        });

        var str = '';
        scoresArray.forEach((highScore) => {
            str += highScore.name + ": " + highScore.score + "\n";
        });
        return str.trim();
    }

    var printHighScores = function() {
        return this.getHighScores().then((scores) => {
            return new Promise((resolve, reject) => {
                this.highScores = scores;
                const str = this.prettyPrint(scores);

                resolve(str);
            });
        });
    };

    var addScore = function(name, numMovesMade) {
        numMovesMade = numMovesMade || 0;
        const item = {
            name: name,
            score: numMovesMade
        };
        this.highScores.push(item);
    };

    var insertHighScore = function(name, numMovesMade) {
        if (!name) {
            console.error("Name was not specified");
            return;
        }

        this.addScore(name, numMovesMade);

        jsonfile.writeFile(filePath, this.highScores, function(err) {
            console.error(err);
        });
    };

    this.printHighScores = printHighScores;
    this.insertHighScore = insertHighScore;
    this.getHighScores = getHighScores;
    this.prettyPrint = prettyPrint;
    this.addScore = addScore;
}

module.exports.HighScores = HighScores;
