var gameboard = require("./board.js");
var highscores = require("./highscores.js");
var helpers = require("./helpers.js");

function Game() {
    this.gameBoard = new gameboard.Board();

    function printCurrentBoard() {
        return this.gameBoard.print();
    }

    var place = function(row, column) {
        const isRowValid = row >= 0 && row <= 2;
        const isColumnValid = column >= 0 && column <= 2;
        const isMoveValid = isRowValid && isColumnValid;
        if (isMoveValid) {
            this.gameBoard.insertNextPiece(row, column);
        }

        return isMoveValid;
    };

    var placeRawInput = function(rawCoordinate) {
        var values = rawCoordinate.split(',');
        if (values.length !== 2) {
            console.error("Enter coordinate with format: 1, 2");
        }

        return this.place(values[0].trim(), values[1].trim());
    };

    var hasWon = function() {
        var model = this.gameBoard.parse();
        if (isHorizontalWin(model)) {
            return true;
        }

        if (isVerticalWin(model)) {
            return true;
        }

        if (isDiagonalWin(model)) {
            return true;
        }

        return false;
    };

    var hasEnded = function() {
        const didWin = this.hasWon();

        const isCats = this.gameBoard.isFull()
        return didWin || isCats;
    };

    var isHorizontalWin = function(model) {
        var isWin = false;
        model.forEach((row) => {
            var line = row.join(',');
            if (checkLineForHorizontalWin(line)) {
                isWin = true;
            }
        });

        return isWin;
    };

    var isVerticalWin = function(model) {
        var isWin = false;
        for (i = 0; i < 3; i++) {
            if ((model[0][i] === helpers.PIECES.o &&
                    model[1][i] === helpers.PIECES.o &&
                    model[2][i] === helpers.PIECES.o) ||
                (model[0][i] === helpers.PIECES.x &&
                    model[1][i] === helpers.PIECES.x &&
                    model[2][i] === helpers.PIECES.x)) {

                isWin = true;
            }
        }

        return isWin;
    }

    var isDiagonalWin = function(model) {
        var isWin = false;

        // left-right
        if ((model[0][0] === helpers.PIECES.x &&
                model[1][1] === helpers.PIECES.x &&
                model[2][2] === helpers.PIECES.x) ||
            (model[0][0] === helpers.PIECES.o &&
                model[1][1] === helpers.PIECES.o &&
                model[2][2] === helpers.PIECES.o)) {

            isWin = true;
        }

        // right-left
        if ((model[0][2] === helpers.PIECES.x &&
                model[1][1] === helpers.PIECES.x &&
                model[2][0] === helpers.PIECES.x) ||
            (model[0][2] === helpers.PIECES.o &&
                model[1][1] === helpers.PIECES.o &&
                model[2][0] === helpers.PIECES.o)) {

            isWin = true;
        }

        return isWin;
    }

    var checkLineForHorizontalWin = function(line) {
        return checkForAllX(line) || checkForAllO(line);
    };

    var checkForAllX = function(line) {
        return line === 'X,X,X';
    };

    var checkForAllO = function(line) {
        return line === 'O,O,O';
    };

    var currentPlayer = function() {
        return this.gameBoard.currentPiece();
    };

    var takeAITurn = function() {
        // prevent modifying the original board
        const startingBoard = new gameboard.Board(this.printCurrentBoard());

        // take the middle for first move
        if (this.gameBoard.isEmpty()) {
            return this.place(1, 1);
        }

        // try a move and take it if it wins
        for (row = 0; row < 3; row++) {
            for (column = 0; column < 3; column++) {
                this.gameBoard = new gameboard.Board(startingBoard.print());
                if (this.gameBoard.isPositionEmpty(row, column)) {

                    this.place(row, column);
                    if (this.hasWon()) {
                        return this.printCurrentBoard();
                    }
                }
            }
        }

        // pick any move
        this.gameBoard = new gameboard.Board(startingBoard.print());
        for (row = 0; row < 3; row++) {
            for (column = 0; column < 3; column++) {
                if (this.gameBoard.isPositionEmpty(row, column)) {
                    return this.place(row, column);
                }
            }
        }
    };

    this.highScores = new highscores.HighScores();

    var printHighScores = function() {
        return this.highScores.printHighScores();
    };

    var insertHighScore = function(name) {
        return this.highScores.insertHighScore(name, this.gameBoard.numMovesMade());
    };

    var reset = function() {
        this.gameBoard = new gameboard.Board();
    };

    this.printCurrentBoard = printCurrentBoard;
    this.place = place;
    this.placeRawInput = placeRawInput;
    this.hasEnded = hasEnded;
    this.checkForAllX = checkForAllX;
    this.currentPlayer = currentPlayer;
    this.takeAITurn = takeAITurn;
    this.hasWon = hasWon;
    this.printHighScores = printHighScores;
    this.insertHighScore = insertHighScore;
    this.reset = reset;
}

module.exports.Game = Game;
