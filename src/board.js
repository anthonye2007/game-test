const helpers = require("./helpers.js");

function Board(inputStr) {
    this.boardStr = inputStr ||
        "-,-,-" + "\n" +
        "-,-,-" + "\n" +
        "-,-,-" + "\n";

    var parse = function(inputBoardStr) {
        inputBoardStr = inputBoardStr || this.boardStr;
        var lines = inputBoardStr.split("\n").slice(0, 3).map((line) => {
            return line.split(",");
        });

        return lines;
    };

    var print = function(boardModel) {
        boardModel = boardModel || this.parse();
        return printBoard(boardModel);
    };

    var insertNextPiece = function(row, column) {
        var model = this.parse();
        model[row][column] = this.currentPiece(model);
        this.boardStr = this.print(model);
    };

    var currentPiece = function(boardModel) {
        boardModel = boardModel || this.parse();
        var numX = countOf(helpers.PIECES.x, boardModel);
        var numO = countOf(helpers.PIECES.o, boardModel);
        return numX - numO > 0 ? helpers.PIECES.o : helpers.PIECES.x;
    };

    var isFull = function() {
        const numEmpty = this.boardStr.split(helpers.PIECES.empty).length - 1;
        return numEmpty === 0;
    };

    var isPositionEmpty = function(row, column) {
        const model = this.parse();
        return model[row][column] === helpers.PIECES.empty;
    };

    var isEmpty = function() {
        const numEmpty = this.boardStr.split(helpers.PIECES.empty).length - 1;
        return numEmpty === 9;
    };

    var numMovesMade = function() {
        const model = this.parse();
        var numX = countOf(helpers.PIECES.x, model);
        return numX;
    };

    this.parse = parse;
    this.print = print;
    this.insertNextPiece = insertNextPiece;
    this.currentPiece = currentPiece;
    this.isFull = isFull;
    this.isEmpty = isEmpty;
    this.isPositionEmpty = isPositionEmpty;
    this.numMovesMade = numMovesMade;
}

var printBoard = function(boardModel) {
    var str = "";
    for (var index in boardModel) {
        var row = boardModel[index] + "\n";
        str += row;
    }

    return str;
};

var countOf = function(str, boardModel) {
    return printBoard(boardModel).split(str).length - 1;
};

module.exports.Board = Board;
