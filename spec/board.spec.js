var gameboard = require("../src/board.js");

describe("Board", () => {
    describe("constructor", () => {
        it("should be defined", () => {
            var board = new gameboard.Board();
            expect(board).toBeDefined();
        });

        it("should set boardStr", () => {
            var expectedString = "blah";
            var board = new gameboard.Board(expectedString);
            expect(board.boardStr).toEqual(expectedString);
        });
    });

    describe("parse", () => {
        var actualArray;
        var board;

        beforeEach(() => {
            var inputBoard =
                "X,X,X" + "\n" +
                "-,-,-" + "\n" +
                "-,-,-" + "\n";
            board = new gameboard.Board(inputBoard);
            actualArray = board.parse();
        });

        it("should parse string into array of 3 items", () => {
            expect(actualArray.length).toBe(3);
        });

        it("should have 3 items in first line", () => {
            expect(actualArray[0].length).toBe(3);
        });

        it("should return array of arrays with correct content", () => {
            var expectedArray = [
                ['X', 'X', 'X'],
                ['-', '-', '-'],
                ['-', '-', '-'],
            ];
            expect(actualArray).toEqual(expectedArray);
        });
    });

    describe("isFull", () => {
        it("should return false for initial board", () => {
            var board = new gameboard.Board();
            expect(board.isFull()).toBe(false);
        });

        it("should return true for full board", () => {
            var initialStr =
                "X,O,X" + "\n" +
                "O,X,X" + "\n" +
                "O,X,O" + "\n";
            var board = new gameboard.Board(initialStr);
            expect(board.isFull()).toBe(true);
        });
    });

    describe("isEmpty", () => {
        it("should return true for initial board", () => {
            var board = new gameboard.Board();
            expect(board.isEmpty()).toBe(true);
        });

        it("should return false for board with one piece", () => {
            var initialStr =
                "X,-,-" + "\n" +
                "-,-,-" + "\n" +
                "-,-,-" + "\n";
            var board = new gameboard.Board(initialStr);
            expect(board.isEmpty()).toBe(false);
        });
    });

    describe("isPositionEmpty", () => {
        it("should return true if position is empty", () => {
            var board = new gameboard.Board();
            expect(board.isPositionEmpty(0, 0)).toBe(true);
        });

        it("should return false if position is full", () => {
            var inputBoard =
                "X,X,X" + "\n" +
                "-,-,-" + "\n" +
                "-,-,-" + "\n";
            var board = new gameboard.Board(inputBoard);
            expect(board.isPositionEmpty(0, 0)).toBe(false);
        });
    });

    describe("numMovesMade", () => {
        it("should return 0 if no pieces are on the board", () => {
            var board = new gameboard.Board();
            expect(board.numMovesMade()).toBe(0);
        });

        it("should return 1 if one X is on the board", () => {
            const inputBoard =
                "X,-,-" + "\n" +
                "O,-,-" + "\n" +
                "-,-,-" + "\n";
            var board = new gameboard.Board(inputBoard);
            expect(board.numMovesMade()).toBe(1);
        });

        it("should return 5 for full board", () => {
            const inputBoard =
                "X,O,X" + "\n" +
                "O,X,X" + "\n" +
                "O,X,O" + "\n";
            var board = new gameboard.Board(inputBoard);
            expect(board.numMovesMade()).toBe(5);
        });
    });
});
