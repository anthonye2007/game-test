const app = require("../src/highscores.js");
const testHelpers = require("./testHelpers.js");

describe("HighScores", () => {
    var highScores;
    beforeEach(() => {
        highScores = new app.HighScores("./spec/highScoresTest.json");
    });

    describe("getHighScores", () => {
        it("calls the callback", (done) => {
            highScores.getHighScores().then((scores) => {
                expect(scores).toBeDefined();
                done();
            });
        });
    });

    describe("printHighScores", () => {
        it("pretty prints a name and score", () => {
            const testArray = [{
                "name": "Player One",
                "score": 0
            }];
            const expectedStr = "Player One: 0";
            var actualStr = highScores.prettyPrint(testArray);
            expect(actualStr).toEqual(expectedStr);
        });

        it("pretty prints multiple entries", () => {
            const testArray = [{
                name: "Player One",
                score: 0
            }, {
                name: "Young Joe",
                score: 50
            }];
            const expectedStr = "Player One: 0\nYoung Joe: 50";
            var actualStr = highScores.prettyPrint(testArray);
            expect(actualStr).toEqual(expectedStr);
        });

        it("sorts with lowest score at the top", () => {
            const expectedStr = "Player One: 0\nBilly: 3\nYoung Joe: 50";
            const testArray = [{
                name: "Player One",
                score: 0
            }, {
                name: "Young Joe",
                score: 50
            }, {
                name: "Billy",
                score: 3
            }];
            expect(highScores.prettyPrint(testArray)).toEqual(expectedStr);
        });
    });

    describe("addScore", () => {
        it("adds an entry to high scores", () => {
            highScores.highScores = [];
            highScores.addScore("Kid", 0);
            expect(highScores.highScores.length).toEqual(1);
        });

        it("adds an entry to high scores with specified name", () => {
            const expectedName = "Billy Bob";
            highScores.addScore(expectedName);
            expect(highScores.prettyPrint().includes(expectedName)).toBe(true);
        });

        it("adds a score equal to number of moves taken", () => {
            highScores.highScores = [];
            const expectedName = "Billy Bob";
            highScores.addScore(expectedName, 5);
            expect(highScores.prettyPrint()).toEqual(expectedName + ": 5");
        });
    });
});
