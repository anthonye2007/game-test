var countOf = function(needle, haystack) {
    return haystack.split(needle).length - 1;
};

module.exports.countOf = countOf;
