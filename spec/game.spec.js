const app = require("../src/game.js");
const gameboard = require("../src/board.js");
const highscores = require("../src/highscores.js");
const helpers = require("../src/helpers.js");
const testHelpers = require("./testHelpers.js");

const EMPTY_BOARD = "-,-,-" + "\n" +
    "-,-,-" + "\n" +
    "-,-,-" + "\n";

describe("Game", () => {
    var game;
    beforeEach(() => {
        game = new app.Game();
        var stubbedFileReader = {};
        stubbedFileReader.readFile = function(file, callback) {};
        game.highScores = new highscores.HighScores(stubbedFileReader);
    });

    it("should be defined", () => {
        expect(app).toBeDefined();
    });

    describe("printCurrentBoard", () => {
        var board;
        beforeEach(() => {
            board = game.printCurrentBoard();
        });

        it("should return a string", () => {
            expect(typeof board).toBe('string');
        });

        it("should have a dash for every space on the board", () => {
            var numDashes = testHelpers.countOf(helpers.PIECES.empty, board);
            expect(numDashes).toBe(9);
        });

        it("should have a new line for every row on the board", () => {
            var numRows = testHelpers.countOf('\n', board);
            expect(numRows).toBe(3);
        });

        it("should print an empty board when initialized", () => {
            expect(game.printCurrentBoard()).toEqual(EMPTY_BOARD);
        });

        it("should print at least one piece", () => {
            var expectedString =
                "X,-,-" + "\n" +
                "-,-,-" + "\n" +
                "-,-,O" + "\n";

            game.gameBoard = new gameboard.Board(expectedString);
            expect(game.printCurrentBoard()).toEqual(expectedString);
        });
    });

    describe("placeRawInput", () => {
        it("should insert a piece at the correct row and column", () => {
            var expectedString =
                "-,-,-" + "\n" +
                "-,X,-" + "\n" +
                "-,-,-" + "\n";
            game.placeRawInput('1, 1');
            expect(game.printCurrentBoard()).toEqual(expectedString);
        });

        it("should return true if the move was valid", () => {
            const isValid = game.placeRawInput('1, 1');
            expect(isValid).toBe(true);
        });

        it("should return false if the move was not valid", () => {
            const isValid = game.placeRawInput('50, 10');
            expect(isValid).toBe(false);
        });
    });

    describe("place", () => {
        it("should be defined", () => {
            expect(game.place).toBeDefined();
        });

        describe("should first insert a X", () => {
            it("in the board", () => {
                var board = game.printCurrentBoard();
                var oldX = testHelpers.countOf(helpers.PIECES.x, board);

                game.place('0', '0');

                var newBoard = game.printCurrentBoard();
                var newX = testHelpers.countOf(helpers.PIECES.x, newBoard);
                expect(newX).toBe(oldX + 1);
            });

            it("at the correct column", () => {
                var expectedString =
                    "-,X,-" + "\n" +
                    "-,-,-" + "\n" +
                    "-,-,-" + "\n";
                game.place('0', '1');
                var board = game.printCurrentBoard();
                expect(board).toEqual(expectedString);
            });

            it("at the correct row and column", () => {
                var expectedString =
                    "-,-,-" + "\n" +
                    "-,X,-" + "\n" +
                    "-,-,-" + "\n";
                game.place('1', '1');
                var board = game.printCurrentBoard();
                expect(board).toEqual(expectedString);
            });
        });

        describe("should guard against invalid", () => {
            it("column input", () => {
                var isValid = game.place('0', '3');
                expect(isValid).toBe(false);
            });

            it("row input", () => {
                var isValid = game.place('-1', '0');
                expect(isValid).toBe(false);
            });
        });

        it("should secondly insert a O", () => {
            game.place('0', '0');
            game.place('1', '1');
            var actualBoard = game.printCurrentBoard();
            var numO = testHelpers.countOf(helpers.PIECES.o, actualBoard);
            expect(numO).toBeGreaterThan(0);
        });

        it("should rotate between X and O", () => {
            game.place('0', '0');
            game.place('1', '1');
            game.place('2', '2');
            var actualBoard = game.printCurrentBoard();
            var numX = testHelpers.countOf(helpers.PIECES.x, actualBoard);
            var numO = testHelpers.countOf(helpers.PIECES.o, actualBoard);
            expect(numX).toBe(2);
            expect(numO).toBe(1);
        });
    });

    describe("hasEnded", () => {
        describe("should handle no parameter", () => {
            it("and return true when current board has won", () => {
                var initialStr =
                    "X,X,X" + "\n" +
                    "-,-,-" + "\n" +
                    "-,-,O" + "\n";
                game.gameBoard = new gameboard.Board(initialStr);

                expect(game.hasEnded()).toBe(true);
            });

            it("and return false when current board has not won", () => {
                var initialStr =
                    "X,X,-" + "\n" +
                    "-,-,-" + "\n" +
                    "-,-,-" + "\n";
                game.gameBoard = new gameboard.Board(initialStr);

                expect(game.hasEnded()).toBe(false);
            });
        });

        describe("vertical -", () => {
            it("should catch a vertical line for O", () => {
                var initialStr =
                    "O,-,-" + "\n" +
                    "O,-,-" + "\n" +
                    "O,-,-" + "\n";
                game.gameBoard = new gameboard.Board(initialStr);

                expect(game.hasEnded()).toBe(true);
            });

            it("should catch a vertical line for O", () => {
                var initialStr =
                    "X,-,-" + "\n" +
                    "X,-,-" + "\n" +
                    "X,-,-" + "\n";
                game.gameBoard = new gameboard.Board(initialStr);

                expect(game.hasEnded()).toBe(true);
            });

            it("should catch the second vertical line", () => {
                var initialStr =
                    "-,O,-" + "\n" +
                    "-,O,-" + "\n" +
                    "-,O,-" + "\n";
                game.gameBoard = new gameboard.Board(initialStr);

                expect(game.hasEnded()).toBe(true);
            });

            it("should catch the third vertical line", () => {
                var initialStr =
                    "-,-,O" + "\n" +
                    "-,-,O" + "\n" +
                    "-,-,O" + "\n";
                game.gameBoard = new gameboard.Board(initialStr);

                expect(game.hasEnded()).toBe(true);
            });
        });

        describe("horizontal -", () => {
            it("3 X in a row", () => {
                var initialStr =
                    "X,X,X" + "\n" +
                    "-,-,-" + "\n" +
                    "-,-,-" + "\n";
                game.gameBoard = new gameboard.Board(initialStr);

                expect(game.hasEnded()).toBe(true);
            });

            it("3 X in a row on second line", () => {
                var initialStr =
                    "-,-,-" + "\n" +
                    "X,X,X" + "\n" +
                    "-,-,-" + "\n";
                game.gameBoard = new gameboard.Board(initialStr);

                expect(game.hasEnded()).toBe(true);
            });

            it("3 O in a row", () => {
                var initialStr =
                    "-,-,-" + "\n" +
                    "O,O,O" + "\n" +
                    "-,-,-" + "\n";
                game.gameBoard = new gameboard.Board(initialStr);

                expect(game.hasEnded()).toBe(true);
            });

            it("3 O in a row but mixed", () => {
                var initialStr =
                    "-,-,-" + "\n" +
                    "O,X,O" + "\n" +
                    "-,-,-" + "\n";
                game.gameBoard = new gameboard.Board(initialStr);

                expect(game.hasEnded()).toBe(false);
            });
        });

        it("should catch cat's game", () => {
            var initialStr =
                "X,O,X" + "\n" +
                "O,X,X" + "\n" +
                "O,X,O" + "\n";
            game.gameBoard = new gameboard.Board(initialStr);

            expect(game.hasEnded()).toBe(true);
        });

        describe("diagonal -", () => {
            describe("should catch a left-right diagonal with all", () => {
                it("Xs", () => {
                    var initialStr =
                        "X,-,-" + "\n" +
                        "-,X,-" + "\n" +
                        "-,-,X" + "\n";
                    game.gameBoard = new gameboard.Board(initialStr);

                    expect(game.hasEnded()).toBe(true);
                });

                it("Os", () => {
                    var initialStr =
                        "O,-,-" + "\n" +
                        "-,O,-" + "\n" +
                        "-,-,O" + "\n";
                    game.gameBoard = new gameboard.Board(initialStr);

                    expect(game.hasEnded()).toBe(true);
                });
            });

            describe("should catch a right-left diagonal with all", () => {
                it("Xs", () => {
                    var initialStr =
                        "-,-,X" + "\n" +
                        "-,X,-" + "\n" +
                        "X,-,-" + "\n";
                    game.gameBoard = new gameboard.Board(initialStr);

                    expect(game.hasEnded()).toBe(true);
                });

                it("Os", () => {
                    var initialStr =
                        "-,-,O" + "\n" +
                        "-,O,-" + "\n" +
                        "O,-,-" + "\n";
                    game.gameBoard = new gameboard.Board(initialStr);

                    expect(game.hasEnded()).toBe(true);
                });
            });
        });
    });

    describe("checkForAllX", () => {
        it("should return true for line with all X", () => {
            var line = 'X,X,X';
            expect(game.checkForAllX(line)).toBe(true);
        });

        it("should return false for line with one X", () => {
            var line = 'X,-,-';
            expect(game.checkForAllX(line)).toBe(false);
        });
    });

    describe("currentPlayer", () => {
        it("should return 'X' on initial move", () => {
            expect(game.currentPlayer()).toEqual(helpers.PIECES.x);
        });

        it("should return 'O' on second move", () => {
            game.placeRawInput("0,0");
            expect(game.currentPlayer()).toEqual(helpers.PIECES.o);
        });
    });

    describe("takeAITurn", () => {
        it("should be defined", () => {
            expect(game.takeAITurn).toBeDefined();
        });

        it("should place another piece on the board", () => {
            game.placeRawInput("0,0");
            game.takeAITurn();

            const resultingBoard = game.printCurrentBoard();
            var numX = testHelpers.countOf(helpers.PIECES.x, resultingBoard);
            var numO = testHelpers.countOf(helpers.PIECES.o, resultingBoard);
            expect(numX).toBe(1);
            expect(numO).toBe(1);
        });

        it("should place a piece in an empty position", () => {
            var initialStr =
                "X,-,-" + "\n" +
                "-,X,-" + "\n" +
                "-,-,O" + "\n";
            game.gameBoard = new gameboard.Board(initialStr);

            game.takeAITurn();
            const resultingBoard = game.printCurrentBoard();
            expect(resultingBoard).not.toEqual(initialStr);
        });

        it("should complete row if have two in row already", () => {
            const initialStr =
                "X,X,-" + "\n" +
                "O,O,-" + "\n" +
                "X,-,-" + "\n";
            game.gameBoard = new gameboard.Board(initialStr);

            game.takeAITurn();
            const resultingBoard = game.printCurrentBoard();

            const expectedStr =
                "X,X,-" + "\n" +
                "O,O,O" + "\n" +
                "X,-,-" + "\n";
            expect(resultingBoard).toEqual(expectedStr);
        });

        it("should place in the middle on first turn", () => {
            var expectedStr =
                "-,-,-" + "\n" +
                "-,X,-" + "\n" +
                "-,-,-" + "\n";
            game.takeAITurn();
            expect(game.printCurrentBoard()).toEqual(expectedStr);
        });
    });

    describe("reset", () => {
        it("clears out the gameboard", () => {
            const initialStr =
                "X,X,-" + "\n" +
                "O,O,O" + "\n" +
                "X,-,-" + "\n";
            game.gameBoard = new gameboard.Board(initialStr);
            game.reset();
            expect(game.printCurrentBoard()).toEqual(EMPTY_BOARD);
        });
    });
});
