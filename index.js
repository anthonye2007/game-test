var app = require("./src/game.js");
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var outputBoard = function() {
    console.log(game.printCurrentBoard());
};

var checkForGameOver = function() {
    outputBoard();
    if (game.hasEnded()) {
        console.log("The game is over!");

        rl.question("Enter your name: ", (answer) => {
            game.insertHighScore(answer);
            console.log("High Scores:");
            game.printHighScores().then((scores) => {
                console.log(scores);
                game.reset();
                startGame();
            });
        });
    }
};

var promptForInput = function() {
    rl.question(game.currentPlayer() + ' -- enter your move (e.g. 0,2): ',
        (answer) => {
            const isValid = game.placeRawInput(answer);
            if (!isValid) {
                console.log("Invalid move: " + answer);
                console.log("  The top left of the board is 0,0");
                console.log("  The bottom right of the board is 2,2");
                return promptForInput();
            }
            checkForGameOver();

            game.takeAITurn();
            checkForGameOver();

            promptForInput();
        });
};

var game = new app.Game();
var startGame = function() {
    console.log("\n\nThis is Tic-Tac-Toe");
    game.takeAITurn();
    outputBoard(game);
    promptForInput();
};

startGame();
